#!/bin/bash

if [[ $# -lt 2 ]]; then
    echo "usage: ${0##*/} PROJECT COMMAND"
    exit 1
fi
PROJECT=${1}
DOCKER_CMD=${2}

SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"

. ${SCRIPT_DIR}/bamboo.conf

# Make sure volumes for dijitso cache and ccache exists
docker volume create --name dijitso-cache-${DOCKER_IMAGE_BASENAME}
docker volume create --name ccache

docker pull ${DOCKER_REPO}

docker run \
       --env HOST_UID=$(id -u) \
       --env HOST_GID=$(id -g) \
       --env FENICS_PYTHON=${FENICS_PYTHON} \
       --env COVERALLS_REPO_TOKEN=${bamboo_COVERALLS_REPO_PASSWORD} \
       --volume ccache:/home/fenics/.ccache \
       --volume dijitso-cache-${DOCKER_IMAGE_BASENAME}:/home/fenics/.cache/dijitso \
       --volume ${bamboo_working_directory}:${BAMBOO_WORKDIR} \
       --workdir /home/fenics \
       --rm \
       --label org.fenicsproject.created_by_bamboo=true \
       ${DOCKER_REPO} \
       "${DOCKER_CMD}"
