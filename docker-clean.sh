#!/bin/bash

# Remove any stopped containers created by Bamboo
docker ps -q \
       --filter status=exited \
       --filter label=org.fenicsproject.created_by_bamboo=true \
    | xargs --no-run-if-empty docker rm

# Remove any containers created by Bamboo that have been running for 3
# hours or more
containers=$(docker ps -q -f status=running -f label=org.fenicsproject.created_by_bamboo=true)
for container in $containers; do
    now=$(date -u "+%s")
    started_at=$(docker inspect --format "{{.State.StartedAt}}" $container)
    started_at=$(date -u --date "$started_at" "+%s")
    seconds=$(($now - $started_at))
    if [ $seconds -gt 10800 ]; then  # more than 3 hours
        docker rm --force $container
    fi
done

# Remove any dangling Docker images
docker images -q --filter dangling=true | xargs --no-run-if-empty docker rmi

# Remove Docker images that are 7 days or older (keep images for master or next)
images=$(docker images --filter label=org.fenicsproject.created_by_bamboo=true)
while read -r line; do
    name=$(echo $line | cut -d" " -f1)
    tag=$(echo $line | cut -d" " -f2)
    now=$(date -u "+%s")
    started_at=$(docker inspect --format "{{.Created}}" $name:$tag)
    started_at=$(date -u --date "$started_at" "+%s")
    seconds=$(($now - $started_at))
    if [ $seconds -gt 604800 ]; then  # more than 7 days old
        image=$(echo $name:$tag | grep -E "quay.io/fenicsproject_dev/|\-builder" | grep -v -E "master|next")
        if [ -n "$image" ]; then
            docker rmi $name:$tag
        fi
    fi
done <<< "$images"
