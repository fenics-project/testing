#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}

# Run tests and copy results to BAMBOO_WORKDIR
cd ${FENICS_SRC_DIR}/fiat/test/regression
DATA_REPO_GIT="" ${FENICS_PYTHON} -B -m pytest -svl --junitxml ${BAMBOO_WORKDIR}/report.xml .
