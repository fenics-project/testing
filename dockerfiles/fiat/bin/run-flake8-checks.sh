#!/bin/bash

set -e

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}

# Run flake8 checks
cd ${FENICS_SRC_DIR}/fiat
#${FENICS_PYTHON} -B -m flake8 --output-file ${BAMBOO_WORKDIR}/flake8.txt .
${FENICS_PYTHON} -B -m flake8 .

# Convert flake8 file to junit format
#flake8_junit ${BAMBOO_WORKDIR}/flake8.txt ${BAMBOO_WORKDIR}/flake8_junit.xml
