#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}

# Run tests and copy results to BAMBOO_WORKDIR
cd ${FENICS_SRC_DIR}/fiat/test/unit
${FENICS_PYTHON} -B -m pytest -svl --cov-report html --cov=FIAT --junitxml ${BAMBOO_WORKDIR}/report.xml .
cp -r htmlcov ${BAMBOO_WORKDIR}/

# Run coveralls (not for Python 2)
if [[ "${FENICS_PYTHON}" != "python2" ]]; then
    coveralls --verbose || true
fi
