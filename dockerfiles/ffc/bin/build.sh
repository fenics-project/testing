#!/bin/bash

set -e

sudo pip3 install --no-cache-dir --upgrade singledispatch networkx pulp pybind11

packages="fiat dijitso ufl COFFEE FInAT tsfc ffc"
for p in $packages; do
    FENICS_PYTHON=python3 fenics-build ${p}
done

# Build google test for testing uflacs
cd ${FENICS_SRC_DIR}/ffc/libs/gtest-1.7.0 && \
    mkdir -p lib && \
    cd lib && \
    cmake .. && \
    make

# Install ffc-factory module (pybind11 wrappers)
cd ${FENICS_SRC_DIR}/ffc
pip3 install --prefix=${FENICS_PREFIX} libs/ffc-factory/
