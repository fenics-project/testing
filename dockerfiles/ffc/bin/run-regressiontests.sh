#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}
export DIJITSO_CACHE_DIR=${DIJITSO_CACHE_DIR:-"${HOME}/.cache/dijitso"}

# Run tests and copy results to BAMBOO_WORKDIR
cd ${FENICS_SRC_DIR}/ffc/test/regression
DATA_REPO_GIT="" ${FENICS_PYTHON} -B test.py
if [ "$?" -ne "0" ]; then
    cp -v error.log ${BAMBOO_WORKDIR}
    exit 1
else
    exit 0
fi
