#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}
export DIJITSO_CACHE_DIR=${DIJITSO_CACHE_DIR:-"${HOME}/.cache/dijitso"}
export DOLFIN_NOPLOT=${DOLFIN_NOPLOT:-1}
export MPLBACKEND="agg"
export PROCS=${PROCS:-1}

# Run tests and copy results to BAMBOO_WORKDIR
cd ${FENICS_SRC_DIR}/dolfinx/python/demo
#${FENICS_PYTHON} -B -m pytest -n${PROCS} -svl --junitxml ${BAMBOO_WORKDIR}/report-regression.xml test.py

# Run only selected demos
python3 documented/poisson/demo_poisson.py
python3 undocumented/elasticity/demo_elasticity.py
cd documented/stokes-taylor-hood
python3 demo_stokes-taylor-hood.py
