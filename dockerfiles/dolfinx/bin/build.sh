#!/bin/bash

set -e

export FENICS_BUILD_TYPE=${FENICS_BUILD_TYPE:-"Developer"}
export VERBOSE=${VERBOSE:-1}
export PROCS=${PROCS:-1}

packages="fiat ufl dijitso ffcx dolfinx/cpp"
for p in $packages; do
    fenics-build ${p}
done
CXX="ccache c++" fenics-build dolfinx/python

# Build C++ unit tests
cd ${FENICS_SRC_DIR}/dolfinx/cpp/build-${FENICS_PYTHON}
make -j${PROCS} unittests

# Build Python demos
cd ${FENICS_SRC_DIR}/dolfinx/python
python3 demo/generate-demo-files.py

# Install other test dependencies (could be added to dev-env image)
python3 -mpip install -v --upgrade --prefix=${FENICS_PREFIX} numba
python3 -mpip install -v --upgrade --prefix=${FENICS_PREFIX} sphinx sphinx_rtd_theme
