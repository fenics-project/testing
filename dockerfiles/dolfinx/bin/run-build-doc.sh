#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}
export DIJITSO_CACHE_DIR=${DIJITSO_CACHE_DIR:-"${HOME}/.cache/dijitso"}

# Build cpp documentation and store as artifact
cd ${FENICS_SRC_DIR}/dolfinx/cpp/doc
doxygen
mkdir -p ${BAMBOO_WORKDIR}/doc/cpp
cp -r html ${BAMBOO_WORKDIR}/doc/cpp

# Build python documentation and store as artifact
cd ${FENICS_SRC_DIR}/dolfinx/python/doc
make html
mkdir -p ${BAMBOO_WORKDIR}/doc/python
cp -r build/html ${BAMBOO_WORKDIR}/doc/python
