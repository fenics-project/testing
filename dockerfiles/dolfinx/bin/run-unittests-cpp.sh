#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}

# Run tests and copy results to BAMBOO_WORKDIR
cd ${FENICS_SRC_DIR}/dolfinx/cpp/build-${FENICS_PYTHON}
ctest -T Test -R unittests
mkdir -p ${BAMBOO_WORKDIR}/cpp_unit
cp -R Testing ${BAMBOO_WORKDIR}/cpp_unit/
