#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}
export DIJITSO_CACHE_DIR=${DIJITSO_CACHE_DIR:-"${HOME}/.cache/dijitso"}
export PROCS=${PROCS:-1}

# Run tests and copy results to BAMBOO_WORKDIR
cd ${FENICS_SRC_DIR}/dolfinx/python/test/unit
${FENICS_PYTHON} -B -m pytest -n${PROCS} -svl --junitxml ${BAMBOO_WORKDIR}/report.xml .
