#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}
export DIJITSO_CACHE_DIR=${DIJITSO_CACHE_DIR:-"${HOME}/.cache/dijitso"}
export DOLFIN_NOPLOT=${DOLFIN_NOPLOT:-1}
export MPLBACKEND="agg"

# Run Poisson demo
cd ${FENICS_SRC_DIR}/dolfinx/cpp/demo/documented/poisson
python3 -mffc -ldolfin Poisson.ufl
cmake .
make
./demo_poisson
mpirun -n 3 ./demo_poisson
