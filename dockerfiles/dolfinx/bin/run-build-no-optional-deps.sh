#!/bin/bash

set -e

export FENICS_BUILD_TYPE=${FENICS_BUILD_TYPE:-"Developer"}
export VERBOSE=${VERBOSE:-1}
export PROCS=${PROCS:-1}

# Turn off all optional dependencies
export CMAKE_EXTRA_ARGS="-DDOLFIN_ENABLE_SLEPC:BOOL=OFF -DDOLFIN_ENABLE_SCOTCH:BOOL=OFF -DDOLFIN_ENABLE_PARMETIS:BOOL=OFF -DDOLFIN_ENABLE_HDF5:BOOL=OFF"

# Clean up build directory
rm -rf ${FENICS_SRC_DIR}/dolfinx/cpp/build-${FENICS_PYTHON}/*

# Recompile
fenics-build dolfinx/cpp

# Build C++ unit tests
cd ${FENICS_SRC_DIR}/dolfinx/cpp/build-${FENICS_PYTHON}
make -j${PROCS} unittests
