#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}
export DIJITSO_CACHE_DIR=${DIJITSO_CACHE_DIR:-"${HOME}/.cache/dijitso"}
export DOLFIN_NOPLOT=${DOLFIN_NOPLOT:-1}
export PROCS=${PROCS:-1}

# Run tests and copy results to BAMBOO_WORKDIR
cd ${FENICS_SRC_DIR}/mshr/build-${FENICS_PYTHON}
ctest -j${PROCS} -T Test -E Python
mkdir -p ${BAMBOO_WORKDIR}/test-other
cp -r Testing ${BAMBOO_WORKDIR}/test-other/
