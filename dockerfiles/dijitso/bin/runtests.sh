#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}
export PYTHON=${FENICS_PYTHON}

# Run tests and copy results to BAMBOO_WORKDIR
cd ${FENICS_SRC_DIR}/dijitso/test && \
    ./runtests.sh && \
    cp -r htmlcov ${BAMBOO_WORKDIR}/ && \
    cp report*.xml ${BAMBOO_WORKDIR}/

# Run coveralls (not for Python 2)
if [[ "${FENICS_PYTHON}" != "python2" ]]; then
    coveralls --verbose || true
fi
