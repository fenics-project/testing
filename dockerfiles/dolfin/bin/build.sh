#!/bin/bash

set -e

export FENICS_BUILD_TYPE=${FENICS_BUILD_TYPE:-"Developer"}
export CMAKE_EXTRA_ARGS=${CMAKE_EXTRA_ARGS:-"-DDOLFIN_ENABLE_CODE_COVERAGE:BOOL=ON"}
export VERBOSE=${VERBOSE:-1}
export PROCS=${PROCS:-1}

packages="fiat ufl dijitso ffc dolfin"
for p in $packages; do
    fenics-build ${p}
done

# Build C++ unit tests and demos
cd ${FENICS_SRC_DIR}/dolfin/build-${FENICS_PYTHON}
make -j${PROCS} unittests
make -j${PROCS} demos

# Build Python demos
cd ${FENICS_SRC_DIR}/dolfin/python
python3 demo/generate-demo-files.py
