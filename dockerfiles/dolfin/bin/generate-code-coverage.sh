#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}

# Clean up
cd ${BAMBOO_WORKDIR}
rm -rf coverage lcov.info
mkdir coverage

# Combine tracefiles
lcov --add-tracefile unittests_cpp.info \
     --add-tracefile unittests_py.info \
     --add-tracefile unittests_py_mpi.info \
     --add-tracefile demo_cpp.info \
     --add-tracefile demo_cpp_mpi.info \
     --add-tracefile demo_py.info \
     --add-tracefile demo_py_mpi.info \
     --output lcov.info || true

# Generate html report
genhtml -o coverage -t "DOLFIN test coverage" lcov.info || LCOVFAILED=1
if [ $LCOVFAILED ]; then
    cat > coverage/index.html <<EOF
<html><head><title>No coverage report</title></head>
<body>No coverage report for DOLFIN</body>
</html>
EOF
fi

# Upload coverage report using coveralls-lcov
cd ${FENICS_SRC_DIR}/dolfin/
coveralls-lcov --repo-token=${COVERALLS_REPO_TOKEN} ${BAMBOO_WORKDIR}/lcov.info || true
