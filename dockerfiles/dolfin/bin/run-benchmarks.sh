#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}
export VERBOSE=${VERBOSE:-1}
export PROCS=${PROCS:-4}

# Build benchmarks
cd ${FENICS_SRC_DIR}/dolfin/build-${FENICS_PYTHON}
cmake -DDOLFIN_ENABLE_BENCHMARKS:BOOL=ON ..
make -j${PROCS} bench

# Use previous bench.log from BAMBOO_WORKDIR if available
if [ -f ${BAMBOO_WORKDIR}/bench.log ]; then
    cp ${BAMBOO_WORKDIR}/bench.log ${FENICS_SRC_DIR}/dolfin/build-${FENICS_PYTHON}/bench/logs
else
    echo "No copying..."
fi

# Run benchmarks
make run_bench

# Plot the result
cd bench
${FENICS_PYTHON} plot.py

# Copy results to BAMBOO_WORKDIR
mkdir -p ${BAMBOO_WORKDIR}/benchmarks
cp index.html *.png ${BAMBOO_WORKDIR}/benchmarks/
cp logs/bench.log ${BAMBOO_WORKDIR}
