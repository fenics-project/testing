#!/bin/bash

set -e

export VERBOSE=${VERBOSE:-1}
export PROCS=${PROCS:-1}

# Build benchmarks
cd ${FENICS_SRC_DIR}/dolfin/build-${FENICS_PYTHON}
cmake -DDOLFIN_ENABLE_BENCHMARKS:BOOL=ON ..
make -j${PROCS} bench
