#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}
export DIJITSO_CACHE_DIR=${DIJITSO_CACHE_DIR:-"${HOME}/.cache/dijitso"}
export DOLFIN_NOPLOT=${DOLFIN_NOPLOT:-1}

# Run tests and copy results to BAMBOO_WORKDIR
cd ${FENICS_SRC_DIR}/dolfin/build-${FENICS_PYTHON}
make run_regressiontests
if [ "$?" -ne "0" ]; then
    cp test/regression/demo.log ${BAMBOO_WORKDIR}
    exit 1
else
    exit 0
fi
