#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}
export DIJITSO_CACHE_DIR=${DIJITSO_CACHE_DIR:-"${HOME}/.cache/dijitso"}
export DOLFIN_NOPLOT=${DOLFIN_NOPLOT:-1}
export PROCS=${PROCS:-1}

# Run tests and copy results to BAMBOO_WORKDIR
cd ${FENICS_SRC_DIR}/dolfin/python/demo
${FENICS_PYTHON} -B -m pytest -n${PROCS} -svl --junitxml ${BAMBOO_WORKDIR}/report-regression.xml test.py

# Generate LCOV fil
cd ${FENICS_SRC_DIR}/dolfin/build-${FENICS_PYTHON}
lcov --directory . --capture --output-file all.info || true
lcov --remove all.info "/usr/*" \
     "${FENICS_PREFIX}/lib/*" \
     "${FENICS_PREFIX}/include/*" \
     "${FENICS_SRC_DIR}/dolfin/build-${FENICS_PYTHON}/*" \
     "${FENICS_SRC_DIR}/dolfin/test/*" \
     "${FENICS_SRC_DIR}/dolfin/demo/*" \
     "${FENICS_SRC_DIR}/dolfin/bench/*" \
     "${FENICS_SRC_DIR}/dolfin/python/test/*" \
     "${FENICS_SRC_DIR}/dolfin/python/demo/*" \
     --output ${BAMBOO_WORKDIR}/demo_py.info || true
