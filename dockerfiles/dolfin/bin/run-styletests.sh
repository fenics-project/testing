#!/bin/bash

export BAMBOO_WORKDIR=${BAMBOO_WORKDIR:-"/data"}
export DIJITSO_CACHE_DIR=${DIJITSO_CACHE_DIR:-"${HOME}/.cache/dijitso"}

# Run tests and copy results to BAMBOO_WORKDIR
cd ${FENICS_SRC_DIR}/dolfin/build-${FENICS_PYTHON}/test/codingstyle
${FENICS_PYTHON} -B -m pytest -svl --junitxml ${BAMBOO_WORKDIR}/report.xml .
